# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale


def register():
    Pool.register(
        sale.SaleAttributeSet,
        sale.SaleAttribute,
        sale.SaleAttributeAttributeSet,
        sale.Sale,
        module='sale_attribute', type_='model')
